package uk.co.cichocki.stemming.exception;

public class LetterExpectedException extends RuntimeException{
    public LetterExpectedException() {
    }

    public LetterExpectedException(String message) {
        super(message);
    }
}
