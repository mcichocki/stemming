package uk.co.cichocki.stemming.utils;

import uk.co.cichocki.stemming.exception.LetterExpectedException;

public class WordWrapper {
    private final String word;

    public WordWrapper(String word) {
        this.word = word.toUpperCase();
    }

    public String get() {
        return word;
    }

    public boolean stemBeforeSuffixContainsVowel(String suffix) {
        if(!word.endsWith(suffix.toUpperCase())){
            return false;
        }

        int idx = word.lastIndexOf(suffix.toUpperCase());
        if(idx < 1) {
            return false;
        }
        for(int i=0;i<idx;++i){
            if(letterIsVowel(i)){
                return true;
            }
        }
        return false;
    }

    public boolean letterIsConsonant(int i) {
        char c = word.charAt(i);
        if(!Character.isLetter(c)){
            throw new LetterExpectedException(c + " is not a letter");
        }
        switch (c) {
            case 'A':
            case 'E':
            case 'I':
            case 'O':
            case 'U':
                return false;
            case 'Y':
                return (i == 0) || !letterIsConsonant(i - 1);
            default:
                return true;
        }
    }

    public boolean letterIsVowel(int i){
        return !letterIsConsonant(i);
    }

}
