package uk.co.cichocki.stemming;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * this class is not thread safe
 * it is assumed that input text won't be excessively long
 */
public class SearchMachine {

    private final Stemmer stemmer;

    private final Logger logger = LogManager.getLogger();

    private Map<String, Map<String, Integer>> inputTextNormalisedStatsCache = new HashMap<>();

    public SearchMachine(Stemmer stemmer) {
        this.stemmer = stemmer;
    }

    public Integer search(String inputText, String searchPhrase) {
        String phraseStem = stemmer.stem(searchPhrase);
        logger.debug("Search phrase {} normalised: {}", searchPhrase, phraseStem);

        return Optional.ofNullable(getCachedNormalisedStats(inputText.replaceAll("\\.","")).get(phraseStem)).orElse(0);
    }

    private Map<String, Integer> getCachedNormalisedStats(String text) {
        Optional<Map<String, Integer>> optional = Optional.ofNullable(inputTextNormalisedStatsCache.get(text));
        return optional.orElseGet(() -> normaliseThenCountOccurencesAndAddToCache(text));
    }

    private Map<String, Integer> normaliseThenCountOccurencesAndAddToCache(String text) {
        String[] words = text.split(" ");
        for (int i = 0; i < words.length; ++i) {
            words[i] = stemmer.stem(words[i]);
        }
        if(logger.isDebugEnabled()){
            String normalised = Arrays.stream(words).collect(Collectors.joining(" "));
            logger.debug("normalised inputText: \"{}\"", normalised);
        }
        Map<String, Integer> occurences = countOccurences(words);
        inputTextNormalisedStatsCache.put(text, occurences);

        return occurences;
    }

    private Map<String, Integer> countOccurences(String[] words){
        Map<String, Integer> map = new HashMap<>();
        for (String word: words) {
            Integer count = Optional.ofNullable(map.get(word)).orElse(0);
            map.put(word, ++count);
        }
        logger.trace("Occurences map calculated: {}", map);
        return map;
    }
}
