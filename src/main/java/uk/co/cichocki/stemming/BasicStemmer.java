package uk.co.cichocki.stemming;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import uk.co.cichocki.stemming.utils.Utils;
import uk.co.cichocki.stemming.utils.WordWrapper;

import java.util.Arrays;
import java.util.Map;
import java.util.TreeMap;

/**
 * based on acceptance criteria I assumed that suffix stripping is
 * enough to meet requirements
 * <p>
 * inspired by http://snowball.tartarus.org/algorithms/porter/stemmer.html
 */
public class BasicStemmer implements Stemmer {

    private final Logger logger = LogManager.getLogger();

    /**
     * Stemming method
     *
     * @param input the word to be normalised
     * @return method returns upper case normalised stem for a given word
     */
    @Override
    public String stem(String input) {
        String upperCase = input.toUpperCase();

        // since we are looking for presence of longest suffix,
        // we want to ensure that map is ordered descending by suffix length
        Map<String, String> substitutions = new TreeMap<String, String>((s1, s2) -> {
            if (s1.length() > s2.length()) {
                return -1;
            } else if (s1.length() < s2.length()) {
                return 1;
            } else {
                return s1.compareTo(s2);
            }
        }) {
            {
                put("SSES", "SS");
                put("IES", "I");
                put("SS", "SS");
                put("S", "");
            }
        };
        String output = suffixStrip(upperCase, substitutions);
        WordWrapper wrapper = new WordWrapper(output);
        String[] step2suffix = {"Y", "LIER", "LI", "LY"};

        for(String suffix : step2suffix) {
            if (wrapper.stemBeforeSuffixContainsVowel(suffix)) {
                output = Utils.replaceLast(wrapper.get(), suffix, "");
            }
        }

        return output;
    }

    private String suffixStrip(String word, Map<String, String> replacements) {
        for (Map.Entry<String, String> entry : replacements.entrySet()) {
            if (word.endsWith(entry.getKey())) {
                String result = Utils.replaceLast(word, entry.getKey(), entry.getValue());
                logger.debug("{} transformed to {}", word, result);
                return result;
            }
        }
        logger.debug("No replacements made initially for input: {}", word);
        return word;
    }

}
