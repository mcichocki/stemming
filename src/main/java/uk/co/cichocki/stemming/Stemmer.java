package uk.co.cichocki.stemming;

public interface Stemmer {
    String stem(String input);
}
