package uk.co.cichocki.stemming;

import org.junit.Test;

import java.util.Arrays;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class BasicStemmerTest {

    @Test
    public void stem() throws Exception {
        Stemmer stemmer = new BasicStemmer();
        String[] input = {"classes", "caresses", "ponies", "ties", "caress", "cats", "whatever", "happy", "sky"};
        String[] expected = {"CLASS", "CARESS", "PONI", "TI", "CARESS", "CAT", "WHATEVER", "HAPP", "SKY"};
        Object[] actual = Arrays.stream(input).map(stemmer::stem).toArray();
        assertThat(actual, is(expected));
    }

}