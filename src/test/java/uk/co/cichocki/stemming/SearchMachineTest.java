package uk.co.cichocki.stemming;

import org.junit.Test;

import java.util.AbstractMap;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;

public class SearchMachineTest {

    @Test
    public void search() throws Exception {
        Stemmer stemmer = new BasicStemmer();
        SearchMachine searchMachine = new SearchMachine(stemmer);
        String text = "Friends are friendlier friendlies that are friendly and classify the friendly classification class. " +
                "Flowery flowers flow through following the flower flows";

        Map<String, Integer> expected = Collections.unmodifiableMap(Stream.of(
                new AbstractMap.SimpleEntry<>("following", 1),
                new AbstractMap.SimpleEntry<>("flow", 2),
                new AbstractMap.SimpleEntry<>("classification", 1),
                new AbstractMap.SimpleEntry<>("class", 1),
                new AbstractMap.SimpleEntry<>("flower", 3),
                new AbstractMap.SimpleEntry<>("friend", 5),
                new AbstractMap.SimpleEntry<>("friendly", 5),
                new AbstractMap.SimpleEntry<>("classes", 1))
                .collect(Collectors.toMap(AbstractMap.SimpleEntry::getKey, AbstractMap.SimpleEntry::getValue)));

        Map<String, Integer> actual = new HashMap<>();

        expected.keySet().forEach(phrase -> actual.put(phrase, searchMachine.search(text, phrase)));

        assertThat(actual, is(expected));

    }

}