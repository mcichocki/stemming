package uk.co.cichocki.stemming.utils;

import org.junit.Before;
import org.junit.Test;
import uk.co.cichocki.stemming.exception.LetterExpectedException;

import static org.junit.Assert.*;

public class WordWrapperTest {

    @Test
    public void stemBeforeSuffixContainsVowel() throws Exception {
        assertFalse(new WordWrapper("bcded").stemBeforeSuffixContainsVowel("ed"));
        assertTrue(new WordWrapper("abcded").stemBeforeSuffixContainsVowel("ed"));
    }

    @Test
    public void consonantsRecognisedAndNotCaseSensitive() throws Exception {
        WordWrapper[] wrappers = {new WordWrapper("BCDFGHJKLMNPQRSTVXZ"), new WordWrapper("bcdfgh")};
        for (WordWrapper wrapper : wrappers) {
            for (int i = 0; i < wrapper.get().length(); ++i) {
                assertTrue(String.format("failed for i=%d, word %s", i, wrapper.get()), wrapper.letterIsConsonant(i));
            }
        }
        assertTrue(new WordWrapper("ay").letterIsConsonant(1));
        assertTrue(new WordWrapper("yacht").letterIsConsonant(0));
    }

    @Test
    public void vowelsRecognisedAndNotCaseSensitive() throws Exception {
        WordWrapper[] wrappers = {new WordWrapper("AEIOU"), new WordWrapper("aeiou")};
        for (WordWrapper wrapper : wrappers) {
            for (int i = 0; i < wrapper.get().length(); ++i) {
                assertTrue(String.format("failed for i=%d, word %s", i, wrapper.get()), wrapper.letterIsVowel(i));
            }
        }
        assertTrue(new WordWrapper("ly").letterIsVowel(1));

    }

    @Test(expected = LetterExpectedException.class)
    public void failsIfCharacterIsNotLetter(){
        new WordWrapper("abc,d").letterIsConsonant(3);
    }
}