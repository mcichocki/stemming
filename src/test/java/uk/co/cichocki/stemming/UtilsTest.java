package uk.co.cichocki.stemming;

import org.junit.Test;
import uk.co.cichocki.stemming.utils.Utils;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.*;

public class UtilsTest {
    @Test
    public void replaceLast() throws Exception {
        String input = "aaabbb";
        String result = Utils.replaceLast(input, "bb", "xx");
        assertThat(result, is("aaabxx"));
    }

}